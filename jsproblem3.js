function sortAlphabetically (inventory) {
    inventory.sort((a,b) => {
        if (a["car_make"].charCodeAt(0) > b["car_make"].charCodeAt(0)) {
            return 1;
        } else if (a["car_make"].charCodeAt(0) < b["car_make"].charCodeAt(0)) {
            return -1;
        }else {
            return 0;
        }
    })
    return inventory;
}

module.exports = sortAlphabetically;