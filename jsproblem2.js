function makeAndModel(inventory,index) {
    let information = {};
    information["make"] = inventory[index]["car_make"];
    information["model"]= inventory[index]["car_model"];
    return information;
}
module.exports = makeAndModel;