
function olderCars(inventory) {
    countOfOlderCars =0;
    olderCarArray =[];
    for (let index =0;index<inventory.length;index++) {
        if (inventory[index]["car_year"] < 2000) {
            countOfOlderCars +=1;
            olderCarArray.push(inventory[index]);
        }
    }
    return [countOfOlderCars,olderCarArray];
}
module.exports = olderCars;